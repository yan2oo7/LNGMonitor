package com.lissic.lng.chart;

import java.util.ArrayList;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint.Align;

public class BarChart extends AbstractDemoChart {
	  
	private double[] y;
	public BarChart(double[] y){
		this.y = y;
	}
	 /**
	   * Returns the chart name.
	   * 
	   * @return the chart name
	   */
	  public String getName() {
	    return "Sales stacked bar chart";
	  }

	  /**
	   * Returns the chart description.
	   * 
	   * @return the chart description
	   */
	  public String getDesc() {
	    return "The monthly sales for the last 2 years (stacked bar chart)";
	  }

	  /**
	   * 自定义X轴坐标；单柱状图
	   * Executes the chart demo.
	   * 
	   * @param context the context
	   * @return the built intent
	   */
	  public GraphicalView execute(Context context) {
		String[] titles = new String[] { "今日数据" };
		
		//增加横坐标x
		List<double[]> x = new ArrayList<double[]>();
		                    
		x.add(new double[] { 1, 2, 3, 4, 5 });
		
		List<double[]> values = new ArrayList<double[]>();

		values.add(y);

		int[] colors = new int[] { Color.CYAN };
		XYMultipleSeriesRenderer renderer = buildBarRenderer(colors);
		setChartSettings(renderer, "LNG车载气瓶参数",
				"X", "Y", 0.5, 12.5, 0, 100, Color.GRAY,
				Color.LTGRAY);     
		renderer.getSeriesRendererAt(0).setDisplayChartValues(true);
		renderer.getSeriesRendererAt(0).setChartValuesTextSize(20);
		//自定义X轴的字符串标签
		String[] test = {"液位","压力","气耗量","真空度","加气次数"};
		for(int i=0;i<5;i++){
			renderer.addXTextLabel(i+1, test[i]);
		}
		
		//如果想要在X轴显示自定义的标签，那么首先要设置renderer.setXLabels(0);  
		//如果不设置为0，那么所设置的Labels会与原X坐标轴labels重叠
		renderer.setXLabels(0);
		renderer.setYLabels(10);
		renderer.setXLabelsAlign(Align.LEFT);
		renderer.setYLabelsAlign(Align.LEFT);
		renderer.setPanEnabled(false, false);
		renderer.setZoomRate(1.1f);
		renderer.setBarSpacing(0.5f);
		// 设置条形图之间的距离
		renderer.setBarSpacing(-5);
		renderer.setBarWidth(70);
		renderer.setInScroll(false);
		renderer.setPanEnabled(false, false);
		renderer.setClickEnabled(false);
		//设置x轴和y轴标签的颜色
		renderer.setXLabelsColor(Color.RED);
		renderer.setYLabelsColor(0,Color.RED);
		renderer.setLabelsTextSize(20);
		//设置图标的标题
		renderer.setChartTitleTextSize(40);
		renderer.setLabelsColor(Color.RED);
		//设置图例的字体大小
		renderer.setLegendTextSize(30);
		
		XYMultipleSeriesDataset dataset = buildDataset(titles, x, values);
		//返回GraphicalView,可以灵活设置也可以仅作为一部分显示在任何activity上.
		return ChartFactory.getBarChartView(context, dataset, renderer, Type.STACKED);
	  }

	}

