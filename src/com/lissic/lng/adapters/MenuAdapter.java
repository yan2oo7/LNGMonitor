package com.lissic.lng.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.lissic.lng.activities.BackupActivity;
import com.lissic.lng.activities.BarChartActivity;
import com.lissic.lng.activities.MeActivity;
import com.lissic.lng.activities.NewsActivity;
import com.lissic.lng.activities.SettingActivity;
import com.lissic.lng.activities.ShowLNGDataActivity;
import com.lissic.lng.entity.User;
import com.lissic.lng.main.R;

public class MenuAdapter{
	
	private ListView listView ;
	private Context context;
	private final String MYSELF = "我的";
	private final String LOOKDATA = "查看LNG数据";
	private final String BACKUP = "数据备份";
	private final String TABLE = "生成报表";
	private final String NEWS = "LNG行业新闻";
	private final String SETTING = "设置";
	
	private String username;
	
	//准备数据
	private  String[] menuContent = new String[]{MYSELF,LOOKDATA,BACKUP,TABLE,NEWS,SETTING};
	private  int[] menuIcons = new int[]{R.drawable.ic_action_myself,R.drawable.ic_action_lookdata,R.drawable.ic_action_backup,R.drawable.ic_action_table,R.drawable.ic_action_news,R.drawable.ic_action_settings};
	
	public MenuAdapter(ListView listView,Context context,String username){
		this.listView = listView;
		this.context = context;
		this.username = username;
	}
	
	/**
	 * listview的适配器
	 */
	public void setMenuAdapter() {
		List<Map<String,Object>> items = new ArrayList<Map<String,Object>>();
		for(int i = 0; i < menuContent.length; i++){
			Map<String, Object> item = new HashMap<String,Object>();
			item.put("menuContent",menuContent[i] );
			item.put("menuIcon", menuIcons[i]);
			items.add(item);
		}
		//创建一个SimpleAdapter
		SimpleAdapter adapter = new SimpleAdapter(context, items, 
									R.layout.menu_item,new String[]{"menuIcon","menuContent"} ,
									new int[]{R.id.menu_icon,R.id.menu_content});
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				//实现各个菜单 的功能
System.out.println(menuContent[position]+"被点击了"+id);
				switch(menuContent[position]){
				case MYSELF://我的
					startMeActivity();
					break;
				case LOOKDATA://查看数据
					startShowDataActivity();
					break; 
				case BACKUP://数据备份
					startBackupActivity();
					break;
				case TABLE://生成报表
					startBarChartActivity();
					break;
				case NEWS://新闻列表
					startNewsActivity();
					break;
				case SETTING://设置
					startSettingActivity();
					break;
				}
				 
			}
			
		});
	}
	
	/**
	 * 开启我的activity
	 */
	public void startMeActivity(){
		Intent intent = new Intent(context, MeActivity.class);
		intent.putExtra("username", username);
		context.startActivity(intent);
	}
	
	/**
	 * 开启新闻列表的activity
	 */
	public void startNewsActivity(){
		Intent intent = new Intent(context, NewsActivity.class);
		context.startActivity(intent);
	}
	
	//设置界面
	public void startSettingActivity(){
		Intent intent = new Intent(context, SettingActivity.class);
		context.startActivity(intent);
	}
	
	//展示数据界面
	public void startShowDataActivity(){
		Intent intent = new Intent(context, ShowLNGDataActivity.class);
		intent.putExtra("username", username);
		context.startActivity(intent);
	}
	//生成报表
	public void startBarChartActivity(){
		Intent intent = new Intent(context, BarChartActivity.class);
		intent.putExtra("username", username);
		context.startActivity(intent);
	}
	//数据备份
	public void startBackupActivity(){
		Intent intent = new Intent(context, BackupActivity.class);
		intent.putExtra("username", username);
		context.startActivity(intent);
	}
}
