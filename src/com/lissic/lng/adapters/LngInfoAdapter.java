package com.lissic.lng.adapters;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.lissic.lng.dao.UserDaoImpl;
import com.lissic.lng.entity.LNG;
import com.lissic.lng.entity.User;

public class LngInfoAdapter{

	private Context context;
	private String username;
	private ListView view;
	private User user;
	private Handler handler ;
	
	public LngInfoAdapter(Context context, String username,ListView view,Handler handler) {
		super();
		this.context = context;
		this.view = view;
		this.username = username;
		this.handler = handler;
	}
	
	String[] item = new String[]{"LNG编号","液位","压力","真空度","气耗量","加气次数"};
	
	public int getUser(){
		new UserDaoImpl(context,handler).queryUser(username);
		return 1;
	}
	
}
