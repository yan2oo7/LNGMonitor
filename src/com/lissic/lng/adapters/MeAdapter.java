package com.lissic.lng.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lissic.lng.main.R;

import android.content.Context;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class MeAdapter {
	
	private Context context;
	private ListView listView;
	
	//准备数据
	private String[] meItem = new String[]{"账号:","角色:","LNG状态:","车辆信息:"};
	private String[] meInfo ;
	//初始化context和listView
	public MeAdapter(Context context, ListView listView) {
		this.context = context;
		this.listView = listView;
	}
	public MeAdapter(Context context, ListView listView,String[] meInfo) {
		this.context = context;
		this.listView = listView;
		this.meInfo = meInfo;
	}
	//设置listview的适配器
	public void setMeAdapter(){
		List<Map<String,String>> items = new ArrayList<Map<String,String>>();
		//给items赋值
		for (int i = 0; i < meInfo.length; i++) {
			Map<String, String> item = new HashMap<String,String>();
			item.put("meItem", meItem[i]);
			item.put("meInfo", meInfo[i]);
			items.add(item);
		}
		//创建一个SimpleAdapter
		SimpleAdapter adapter = new SimpleAdapter(context, items, R.layout.me_item, new String[]{"meItem","meInfo"}, new int[]{R.id.tv_item,R.id.tv_info});
		listView.setAdapter(adapter);
	}
	
	
}
