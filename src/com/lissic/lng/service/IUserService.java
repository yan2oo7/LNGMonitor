package com.lissic.lng.service;

import com.lissic.lng.entity.User;

public interface IUserService {
	//增加用户
	public int addUser(User user);
	//删除用户
	public int deleteUser(int id);
	//更新用户的信息
	public int updateUser(User user);
	//查看用户的相关信息
	public User queryUser(String username);
}
