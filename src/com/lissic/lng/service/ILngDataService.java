package com.lissic.lng.service;

import com.lissic.lng.entity.LNG;


public interface ILngDataService {
	//添加lng气瓶的数据
	public int addLngData(LNG lng);
	
	//删除lng气瓶的数据
	public int deleteLngData(int lng_id);
	
	//修改lng气瓶参数
	public int updateLngData(LNG lng);
	
	//查询lng气瓶的数据
	public LNG queryLngData(int lng_id);
}
