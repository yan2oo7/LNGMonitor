package com.lissic.lng.service;

import android.content.Context;
import android.os.Handler;

import com.lissic.lng.dao.UserDaoImpl;
import com.lissic.lng.entity.User;

public class UserServiceImpl implements IUserService {
	private UserDaoImpl mius;
	public UserServiceImpl(Context context,Handler handler){
		mius = new UserDaoImpl(context,handler);
	}
	@Override
	public int addUser(User user) {
		return mius.addUser(user);
	}

	@Override
	public int deleteUser(int id) {
		return mius.deleteUser(id);
	}

	@Override
	public int updateUser(User user) {
		return mius.updateUser(user);
	}

	@Override
	public User queryUser(String username) {
		return mius.queryUser(username);
	}

}
