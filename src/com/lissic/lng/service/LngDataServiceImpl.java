package com.lissic.lng.service;

import com.lissic.lng.entity.LNG;


public class LngDataServiceImpl implements ILngDataService {

	private ILngDataService milds;
	public LngDataServiceImpl(){
		milds = new LngDataServiceImpl();
	}
	
	@Override
	public int addLngData(LNG lng) {
		return milds.addLngData(lng);
	}

	@Override
	public int deleteLngData(int lng_id) {
		return milds.deleteLngData(lng_id);
	}

	@Override
	public int updateLngData(LNG lng) {
		return milds.updateLngData(lng);
	}

	@Override
	public LNG queryLngData(int lng_id) {
		return milds.queryLngData(lng_id);
	}

}
