package com.lissic.lng.activities;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.lissic.lng.adapters.LngInfoAdapter;
import com.lissic.lng.adapters.MenuAdapter;
import com.lissic.lng.entity.LNG;
import com.lissic.lng.entity.User;
import com.lissic.lng.main.R;

public class MainActivity extends Activity implements OnTouchListener{
	//主内容布局
	private View content;
	//menu布局
	private View menu;
	private SlidingActivity slidingActivity;
	private MenuAdapter menuAdapter;
	private LngInfoAdapter lngAdapter;
	ListView lngListView;
	//退出应用的标志
	private static boolean isExit = false;
	
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			
			User user = (User) msg.obj;
			List<LNG> lngs = user.getLngs();
			
			List<Map<String,String>> lists = new ArrayList<Map<String,String>>();
			String[] num = new String[lngs.size()];
			String[] yw = new String[lngs.size()];
			String[] yl = new String[lngs.size()];
			String[] zkd = new String[lngs.size()];
			String[] qhl = new String[lngs.size()];
			String[] jqcs = new String[lngs.size()];
			for (int i = 0; i < lngs.size(); i++) {
				num[i] = lngs.get(i).getNum()+"";
				yw[i] = lngs.get(i).getYw();
				yl[i] = lngs.get(i).getYl();
				zkd[i] = lngs.get(i).getZkd();
				qhl[i] = lngs.get(i).getQhl();
				jqcs[i] = lngs.get(i).getJqcs();
			}
			for (int j = 0; j < jqcs.length; j++) {
				Map<String, String> map = new HashMap<>();
				map.put("num", num[j]);
				map.put("yw", yw[j]);
				map.put("yl", yl[j]);
				map.put("zkd", zkd[j]);
				map.put("qhl", qhl[j]);
				map.put("jqcs", jqcs[j]);  
				lists.add(map);
			}
			
			SimpleAdapter sa = new SimpleAdapter(MainActivity.this, lists, R.layout.showlng_item, 
					new String[]{"num","yw","yl","zkd","qhl","jqcs"}, new int[]{R.id.id,R.id.yw,R.id.yl,R.id.zkd,R.id.qhl,R.id.jqcs});
			lngListView.setAdapter(sa);
		};
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.setTitle("LNG车载气瓶信息");
		initView();
		//设置内容的触摸监听事件
		content.setOnTouchListener(this);
	}

	private void initView() {
		//加载菜单资源
		ListView menuListView = (ListView) findViewById(R.id.menus);
		menuAdapter = new MenuAdapter(menuListView, MainActivity.this,getIntent().getStringExtra("username"));
		//加载内容资源
		lngListView = (ListView) findViewById(R.id.lv);
		lngAdapter = new LngInfoAdapter(MainActivity.this, getIntent().getStringExtra("username"),lngListView,handler);
		//初始化各个参数
		content = findViewById(R.id.content);
		menu = findViewById(R.id.menu);
		//活动页面
		slidingActivity = new SlidingActivity(this,content,menu);
		slidingActivity.initValues();
		//设置菜单项的数据
		menuAdapter.setMenuAdapter();
		
		int result = lngAdapter.getUser();
		
	}
	

	/**
	 * 实现content的触摸事件
	 */
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return slidingActivity.onTouch(v, event);
	}
	
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), "再按一次退出程序",
                    Toast.LENGTH_SHORT).show();
        } else {
            finish();
            System.exit(0);
        }
    }
}
