package com.lissic.lng.activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.lissic.lng.adapters.LngInfoAdapter;
import com.lissic.lng.adapters.MeAdapter;
import com.lissic.lng.entity.User;
import com.lissic.lng.main.R;

public class MeActivity extends Activity implements OnClickListener{
	
	private Button btn_return,btn_logout,btn_price,btn_business,btn_switchAccount,btn_forgetUsername;
	private ListView listView;
	private MeAdapter meAdapter;
	private Intent intent;
	private String username;
	private LngInfoAdapter lngAdapter;
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			User user = (User) msg.obj;
			String[] meInfo = new String[]{user.getUsername(),user.getIsAdamin()==0?"普通用户":"管理员","良好",user.getCarId()};
			meAdapter = new MeAdapter(getApplicationContext(), listView, meInfo);
			meAdapter.setMeAdapter();
		};
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_me);
		//初始化控件
		initView();
		//设置监听事件
		setListener();
	}
	
	private void setListener() {
		btn_return.setOnClickListener(this);
		btn_logout.setOnClickListener(this);
		btn_price.setOnClickListener(this);
		btn_business.setOnClickListener(this);
		btn_switchAccount.setOnClickListener(this);
		btn_forgetUsername.setOnClickListener(this);
	}

	private void initView() {
		listView = (ListView) findViewById(R.id.me);
		btn_return = (Button) findViewById(R.id.btn_return);
		btn_logout = (Button) findViewById(R.id.btn_logout);
		btn_price = (Button) findViewById(R.id.btn_price);
		btn_business = (Button) findViewById(R.id.btn_business);
		btn_switchAccount = (Button) findViewById(R.id.btn_switchAccount);
		btn_forgetUsername = (Button) findViewById(R.id.btn_forgetUsername);
		username = getIntent().getStringExtra("username");
		lngAdapter = new LngInfoAdapter(getApplicationContext(),username,listView,handler);
		lngAdapter.getUser();
	}

	/**
	 * 实现按钮的点击事件
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_return://返回上一个界面
			back();
			break;
		case R.id.btn_logout://退出登录
			logout();
			break;
		case R.id.btn_price://lng价格界面
			startPriceActivity();
			break;
		case R.id.btn_business://行业新闻界面
			startNewsActivity();
			break;
		case R.id.btn_switchAccount://切换账户（登录界面）
			logout();
			break;
		case R.id.btn_forgetUsername://忘记用户名和密码
			//TODO
			break;
		}
	}
	
	/**
	 * 返回上一个界面
	 */
	public void back(){
		MeActivity.this.finish();
	}
	
	/**
	 * 退出登录
	 */
	public void logout(){
		intent = new Intent(getApplicationContext(), LoginActivity.class);
		startActivity(intent);
		MeActivity.this.finish();
	}
	
	/**
	 * 开启行业直通车的activity
	 */
	public void startNewsActivity(){
		Intent intent = new Intent(this, LngBussines.class);
		this.startActivity(intent);
	}
	
	/**
	 * 开启LNG价格
	 */
	public void startPriceActivity(){
		Intent intent = new Intent(this, LNGPriceActivity.class);
		this.startActivity(intent);
	}
}
