package com.lissic.lng.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lissic.lng.main.R;


public class NewsDetailActivity extends Activity {

	private WebView newsContent;
	private ProgressBar pb;
	private String contentUrl;
	private TextView loading3;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_newdetails);
		initView();
		WebSettings settings = newsContent.getSettings();
		settings.setBuiltInZoomControls(true);  
		newsContent.loadUrl(contentUrl);
		newsContent.setWebViewClient(new webViewClient());
	}
	
	private void initView() {
		newsContent = (WebView) findViewById(R.id.newsContent);
		pb = (ProgressBar) findViewById(R.id.pb_newDetail);
		loading3 = (TextView) findViewById(R.id.loading3);
		//获取内容链接地址和标题
		getURL();
	}
	
	public void getURL(){
		contentUrl = getIntent().getExtras().getCharSequence("contentUrl").toString();
	}
	
	public void back(View v){
		Intent intent = new Intent(getApplicationContext(), NewsActivity.class);
		startActivity(intent);
		NewsDetailActivity.this.finish();
	}
	
	 @Override   
	    //设置回退    
	    //覆盖Activity类的onKeyDown(int keyCoder,KeyEvent event)方法    
	    public boolean onKeyDown(int keyCode, KeyEvent event) {    
	        if ((keyCode == KeyEvent.KEYCODE_BACK) && newsContent.canGoBack()) {    
	        	newsContent.goBack(); //goBack()表示返回WebView的上一页面    
	            return true;    
	        }    
	        finish();//结束退出程序  
	        return false;    
	    }    
		        
	    //Web视图    
	    private class webViewClient extends WebViewClient {  
	    	
	        public boolean shouldOverrideUrlLoading(WebView view, String url) {    
	            view.loadUrl(url);    
	            return true;    
	        }  
	        
	        @Override
	        public void onPageFinished(WebView view, String url) {
	        	super.onPageFinished(view, url);
	        	pb.setVisibility(View.GONE);
	        	loading3.setVisibility(View.GONE);
	        }
	        
	    }    
}
