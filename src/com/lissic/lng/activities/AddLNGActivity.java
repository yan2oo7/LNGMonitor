package com.lissic.lng.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.lissic.lng.dao.LngDaoImpl;
import com.lissic.lng.entity.LNG;
import com.lissic.lng.main.R;

public class AddLNGActivity extends Activity{
	
	private EditText lngnum,lngyw,lngyl,lngzkd,lngqhl,lngjqcs;
	private String id,yw,yl,zkd,qhl,jqcs;
	private LNG lng;
	private String username;
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			String result = (String) msg.obj;
			if(result.equals("success")){
				//显示意图启动另一个activity
				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
				Toast.makeText(getApplicationContext(), "添加成功^()^", 1).show();
				intent.putExtra("username", username);
				startActivity(intent);
				AddLNGActivity.this.finish();
			}else if(result.equals("fail")){
				Toast.makeText(getApplicationContext(), "添加失败....", 1).show();
			}else if(result.equals("disconnect")){
				Toast.makeText(getApplicationContext(), "网络未连接，请检查网络。。。", 1).show();
			}
		};
	};
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addlng);
		this.setTitle("添加LNG数据");
		initView();
	}
	private void initView() {
		lngnum = (EditText) findViewById(R.id.add_lngId);
		lngyw = (EditText) findViewById(R.id.add_lngyw);
		lngyl = (EditText) findViewById(R.id.add_lngyl);
		lngzkd = (EditText) findViewById(R.id.add_lngzkd);
		lngqhl = (EditText) findViewById(R.id.add_lngqhl);
		lngjqcs = (EditText) findViewById(R.id.add_lngjqcs);
		
		username = getIntent().getStringExtra("username");
		
	}
	
	public void add(View v){
		id = lngnum.getText().toString();
		yw = lngyw.getText().toString();
		yl = lngyl.getText().toString();
		zkd = lngzkd.getText().toString();
		qhl = lngqhl.getText().toString();
		jqcs = lngjqcs.getText().toString();
		lng = new LNG(Integer.parseInt(id), yw, yl, zkd, qhl, jqcs);
		LngDaoImpl lngDaoImpl = new LngDaoImpl(this, username,handler);
		lngDaoImpl.addLngData(lng);
	}
	public void reset(View v){
		lngnum.setText("");
		lngyw.setText("");
		lngyl.setText("");
		lngzkd.setText("");
		lngqhl.setText("");
		lngjqcs.setText("");
	}
}
