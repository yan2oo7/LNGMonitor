package com.lissic.lng.activities;

import com.lissic.lng.main.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

public class LNGPriceActivity extends Activity{

	private WebView webview;
	private ProgressBar pb2;
	private TextView loading2;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lngprice);
		this.setTitle("LNG价格");
		webview = (WebView) findViewById(R.id.price);
		pb2 = (ProgressBar) findViewById(R.id.pb2);
		loading2 = (TextView) findViewById(R.id.loading2);
		WebSettings settings = webview.getSettings();
		settings.setBuiltInZoomControls(true);  
		webview.loadUrl("http://index.sci99.com/oil-887.html");
		webview.setWebViewClient(new webViewClient());
	}
	 @Override   
	    //设置回退    
	    //覆盖Activity类的onKeyDown(int keyCoder,KeyEvent event)方法    
	    public boolean onKeyDown(int keyCode, KeyEvent event) {    
	        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {    
	            webview.goBack(); //goBack()表示返回WebView的上一页面    
	            return true;    
	        }    
	        finish();//结束退出程序  
	        return false;    
	    }    
		        
	    //Web视图    
	    private class webViewClient extends WebViewClient {  
	    	
	        public boolean shouldOverrideUrlLoading(WebView view, String url) {    
	            view.loadUrl(url);    
	            return true;    
	        }  
	        
	        @Override
	        public void onPageFinished(WebView view, String url) {
	        	super.onPageFinished(view, url);
	        	pb2.setVisibility(View.GONE);
	        	loading2.setVisibility(View.GONE);
	        }
	        
	    }    
}
