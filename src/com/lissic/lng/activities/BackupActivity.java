package com.lissic.lng.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.lissic.lng.adapters.LngInfoAdapter;
import com.lissic.lng.entity.LNG;
import com.lissic.lng.entity.User;
import com.lissic.lng.main.R;

public class BackupActivity extends Activity{
	
	private TextView name,sex,tel,address,carId,pwd,isAdamin;
	private Button backUser,backLNG;
	private ListView lng;
	private String username;
	private LngInfoAdapter lngAdapter;
	private SharedPreferences msp;
	private User user;
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			user = (User) msg.obj;
			List<LNG> lngs = user.getLngs();
			List<Map<String,String>> lists = new ArrayList<Map<String,String>>();
			String[] num = new String[lngs.size()];
			String[] yw = new String[lngs.size()];
			String[] yl = new String[lngs.size()];
			String[] zkd = new String[lngs.size()];
			String[] qhl = new String[lngs.size()];
			String[] jqcs = new String[lngs.size()];
			for (int i = 0; i < lngs.size(); i++) {
				num[i] = lngs.get(i).getNum()+"";
				yw[i] = lngs.get(i).getYw();
				yl[i] = lngs.get(i).getYl();
				zkd[i] = lngs.get(i).getZkd();
				qhl[i] = lngs.get(i).getQhl();
				jqcs[i] = lngs.get(i).getJqcs();
			}
			for (int j = 0; j < jqcs.length; j++) {
				Map<String, String> map = new HashMap<>();
				map.put("num", num[j]);
				map.put("yw", yw[j]);
				map.put("yl", yl[j]);
				map.put("zkd", zkd[j]);
				map.put("qhl", qhl[j]);
				map.put("jqcs", jqcs[j]);
				lists.add(map);
			}
			
			SimpleAdapter sa = new SimpleAdapter(getApplicationContext(), lists, R.layout.showlng_item, 
					new String[]{"num","yw","yl","zkd","qhl","jqcs"}, new int[]{R.id.id,R.id.yw,R.id.yl,R.id.zkd,R.id.qhl,R.id.jqcs});
			lng.setAdapter(sa);
			
			name.setText(username);
			sex.setText(user.getSex());
			tel.setText(user.getTel());
			address.setText(user.getAddress());
			pwd.setText(user.getPassword());
			isAdamin.setText((user.getIsAdamin()==0?"普通用户":"管理员"));
			carId.setText(user.getCarId());
			msp = getSharedPreferences(username+"_backupUser", MODE_PRIVATE); 
			Editor editor = msp.edit();
			editor.putString("username", username);
			editor.putString("password", user.getPassword());
			editor.putString("sex", user.getSex());
			editor.putString("tel", user.getTel());
			editor.putString("address", user.getAddress());
			editor.putString("carId", user.getCarId());
			editor.putString("isAdamin", user.getIsAdamin()==0?"普通用户":"管理员");
			editor.commit();
			msp = getSharedPreferences(username+"_backupLNG", MODE_PRIVATE); 
			Editor edit = msp.edit();
			for (int i=0; i<lngs.size(); i++) {
				Set<String> l = new HashSet<String>();
				LNG lng = lngs.get(i);
				l.add(lng.getId()+"");
				l.add(lng.getYw());
				l.add(lng.getYl());
				l.add(lng.getZkd());
				l.add(lng.getQhl());
				l.add(lng.getJqcs());
				edit.putStringSet("lng"+i, l);
			}
			edit.commit();
		};
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_backup);
		this.setTitle("数据备份");
		initView();
		setListener();
		
	}
	
	private void setListener() {
		backupUser();
		backupLNG();
		
	}
	private void backupLNG() {
		
		backLNG.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(BackupActivity.this);
				builder.setTitle("提示")
					.setMessage("信息保存目录：/data/data/com.lissic.lng.main/shared_prefs/backupUser.xml")
					.setPositiveButton("确定", null).show();
			}
		});
	}
	private void backupUser() {
		
		backUser.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(BackupActivity.this);
				builder.setTitle("提示")
					.setMessage("信息保存目录：/data/data/com.lissic.lng.main/shared_prefs/backupLNG.xml")
					.setPositiveButton("确定", null).show();
			}
		});
	}
	private void initView() {
		username = getIntent().getStringExtra("username");
		lng = (ListView) findViewById(R.id.LNGBackup);
		name = (TextView) findViewById(R.id.tv_username);
		pwd = (TextView) findViewById(R.id.tv_password);
		sex = (TextView) findViewById(R.id.tv_sex);
		tel = (TextView) findViewById(R.id.tv_tel);
		address = (TextView) findViewById(R.id.tv_address);
		carId = (TextView) findViewById(R.id.tv_carId);
		isAdamin = (TextView) findViewById(R.id.tv_isAdamin);
		backUser = (Button) findViewById(R.id.userBackup);
		backLNG = (Button) findViewById(R.id.lngBackup);
		lngAdapter = new LngInfoAdapter(getApplicationContext(),username,lng,handler);
		lngAdapter.getUser();
	}
}
