package com.lissic.lng.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.achartengine.GraphicalView;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.lissic.lng.adapters.LngInfoAdapter;
import com.lissic.lng.chart.BarChart;
import com.lissic.lng.entity.LNG;
import com.lissic.lng.entity.User;
import com.lissic.lng.main.R;

public class BarChartActivity extends Activity{
	
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			User user = (User) msg.obj;
			List<LNG> lngs = user.getLngs();
			
			List<Map<String,String>> lists = new ArrayList<Map<String,String>>();
			String[] id = new String[lngs.size()];
			String[] yw = new String[lngs.size()];
			String[] yl = new String[lngs.size()];
			String[] zkd = new String[lngs.size()];
			String[] qhl = new String[lngs.size()];
			String[] jqcs = new String[lngs.size()];
			for (int i = 0; i < lngs.size(); i++) {
				id[i] = lngs.get(i).getId()+"";
				yw[i] = lngs.get(i).getYw();
				yl[i] = lngs.get(i).getYl();
				zkd[i] = lngs.get(i).getZkd();
				qhl[i] = lngs.get(i).getQhl();
				jqcs[i] = lngs.get(i).getJqcs();
			}
			for (int j = 0; j < jqcs.length; j++) {
				Map<String, String> map = new HashMap<>();
				map.put("id", id[j]);
				map.put("yw", yw[j]);
				map.put("yl", yl[j]);
				map.put("zkd", zkd[j]);
				map.put("qhl", qhl[j]);
				map.put("jqcs", jqcs[j]);
				lists.add(map);
			}
			double[] y = new double[]{Double.parseDouble(yw[0]),Double.parseDouble(yl[0]),Double.parseDouble(zkd[0]),Double.parseDouble(qhl[0]),Double.parseDouble(jqcs[0])};
			BarChart bar = new BarChart(y);
			GraphicalView graphicalView = bar.execute(BarChartActivity.this);
			LinearLayout layout=(LinearLayout)findViewById(R.id.linearlayout);
			layout.removeAllViews();
			layout.setBackgroundColor(Color.BLACK);
			layout.addView(graphicalView, new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
		};
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_barchart);
		//加载数据
		LngInfoAdapter lngInfoAdapter = new LngInfoAdapter(getApplicationContext(), getIntent().getStringExtra("username"), null, handler);
		lngInfoAdapter.getUser();
		
	}
}
