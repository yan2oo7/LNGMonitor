package com.lissic.lng.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.lissic.lng.main.R;
import com.lissic.lng.utils.HttpUtil;

/**
 * 这是程序的入口
 * @author lissic
 *
 */
public class LoginActivity extends Activity {

	private EditText met_username;
	private EditText met_password;
	private EditText met_lisence;
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			String result = (String) msg.obj;
			if(result.equals("success")){
				//显示意图启动另一个activity
				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
				//将登录的信息传到主界面
				String username = met_username.getText().toString();
				intent.putExtra("username", username);
				startActivity(intent);
				LoginActivity.this.finish();
			}else if(result.equals("fail")){
				Toast.makeText(getApplicationContext(), "密码错误...", 1).show();
			}else if(result.equals("notexits")){
				Toast.makeText(getApplicationContext(), "用户不存在...", 1).show();
			}else if(result.equals("0")){
				Toast.makeText(getApplicationContext(), "服务器忙，稍后重试...", 1).show();
			}else{
				Toast.makeText(getApplicationContext(), "网络未连接，请检查网络...", 1).show();
			}
		};
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		//找到我们关心的控件
		met_username = (EditText) findViewById(R.id.et_username);
		met_password = (EditText) findViewById(R.id.et_password);
		met_lisence = (EditText) findViewById(R.id.et_lisence);
	}
	
	/**
	 * 登录按钮的点击时间
	 * @param view
	 */
	public void login(View view){
		
		final String username = met_username.getText().toString().trim();
		final String password = met_password.getText().toString().trim();
		String lisence = met_lisence.getText().toString().trim();
		if(TextUtils.isEmpty(username)){
			met_username.setError("用户名不能为空");
			return ;
		}else if(TextUtils.isEmpty(password)){
			met_password.setError("密码不能为空");
			return ;
		}else if(TextUtils.isEmpty(lisence)){
			met_lisence.setError("授权码无效");
			return ;
		}else{
			//登录成功后执行的动作
			HttpUtil.loginSuccess(username, password,lisence, handler);
		}
		
	}
	
    
    //取消按钮的监听事件
	public void  cancel(View view){
		met_username.setText("");
		met_password.setText("");
		met_lisence.setText("");
	}
	
	//注册用户的按钮的监听事件
	public void regist(View view){
		Intent intent = new Intent(getApplicationContext(),RegistActivity.class);
		startActivity(intent);
	}
	
}
