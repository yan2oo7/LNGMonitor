package com.lissic.lng.activities;


import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import com.lissic.lng.entity.LNG;
import com.lissic.lng.entity.User;
import com.lissic.lng.main.R;
import com.lissic.lng.service.UserServiceImpl;

public class RegistActivity extends Activity {
	
	private EditText regist_name,regist_pwd,regist_carId,regist_tel,regist_addr,lngId,lngyw,lngyl,lngzkd,lngqhl,lngjqcs;
	private RadioButton female,male;
	private RadioGroup radio_sex;
	private String name,pwd,tel,addr,carId,sex,lngID,lngYW,lngYL,lngZKD,lngQHL,lngJQCS;
	private Handler hanlder = new Handler(){
		public void handleMessage(android.os.Message msg) {
			String result = (String) msg.obj;
			if(result.equals("success")){
				//显示意图启动另一个activity
				Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
				Toast.makeText(getApplicationContext(), "注册成功^()^", 1).show();
				startActivity(intent);
				RegistActivity.this.finish();
			}else if(result.equals("fail")){
				Toast.makeText(getApplicationContext(), "添加失败....", 1).show();
			}else if(result.equals("disconnect")){
				Toast.makeText(getApplicationContext(), "网络未连接，请检查网络。。。", 1).show();
			}
		};
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_regist);
		//初始化控件
		initView();
		//设置监听器
		setListener();
	}
	
	private void setListener() {
		radio_sex.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if(checkedId==male.getId()){
					sex = "男";
				}else{
					sex = "女";
				}
			}
		});
	}

	private void initView() {
		regist_name = (EditText) findViewById(R.id.regist_name);
		regist_pwd = (EditText) findViewById(R.id.regist_pwd);
		regist_carId = (EditText) findViewById(R.id.regist_carId);
		regist_tel = (EditText) findViewById(R.id.regist_tel);
		regist_addr = (EditText) findViewById(R.id.regist_address);
		female = (RadioButton) findViewById(R.id.regist_female);
		male = (RadioButton) findViewById(R.id.regist_male);
		radio_sex = (RadioGroup) findViewById(R.id.sex);
		lngId = (EditText) findViewById(R.id.regist_lngId);
		lngyw = (EditText) findViewById(R.id.regist_lngyw);
		lngyl = (EditText) findViewById(R.id.regist_lngyl);
		lngzkd = (EditText) findViewById(R.id.regist_lngzkd);
		lngqhl = (EditText) findViewById(R.id.regist_lngqhl);
		lngjqcs = (EditText) findViewById(R.id.regist_lngjqcs);
	}
	
	public void reset(View v){
		regist_name.setText("");
		regist_pwd.setText("");
		regist_carId.setText("");
		regist_tel.setText("");
		regist_addr.setText("");
	}
	
	public void regist(View v){
		name = regist_name.getText().toString();
		pwd = regist_pwd.getText().toString();
		carId = regist_carId.getText().toString();
		tel = regist_tel.getText().toString();
		addr= regist_addr.getText().toString();
		lngID = lngId.getText().toString();
		lngYW = lngyw.getText().toString();
		lngYL = lngyl.getText().toString();
		lngZKD = lngzkd.getText().toString();
		lngQHL = lngqhl.getText().toString();
		lngJQCS = lngjqcs.getText().toString();
		if(name.equals("")){
			regist_name.setError("用户名不能为空");
			return ;
		}else if(pwd.equals("")){
			regist_pwd.setError("密码不能为空");
			return ;
		}else if(carId.equals("")){
			regist_carId.setError("车牌不能为空");
			return ;
		}else if(tel.equals("")){
			regist_tel.setError("电话不能为空");
			return ;
		}else if(addr.equals("")){
			regist_addr.setError("地址不能为空");
			return ;
		}else if(lngID.equals("")||lngYW.equals("")||lngYL.equals("")||lngZKD.equals("")||lngQHL.equals("")||lngJQCS.equals("")){
			Toast.makeText(getApplicationContext(), "请填写完整的LNG信息...", 1).show();
			return ;
		}
		LNG lng = new LNG(Integer.parseInt(lngID), lngYW, lngYL, lngZKD, lngQHL, lngJQCS);
		List<LNG> list = new ArrayList<>();
		list.add(lng);
		User user = new User(-1, name, pwd, sex,tel, addr, carId, null, 0, list);
System.out.println(user);
		UserServiceImpl us = new UserServiceImpl(getApplicationContext(),hanlder);
		int flag = us.addUser(user);
	}
}
