package com.lissic.lng.activities;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.lissic.lng.adapters.LngInfoAdapter;
import com.lissic.lng.adapters.ShowLngDataAdapter;
import com.lissic.lng.entity.LNG;
import com.lissic.lng.entity.User;
import com.lissic.lng.main.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.ResponseHandlerInterface;

public class ShowLNGDataActivity extends Activity {
	
	private ListView lv;
	private String username;
	List<Map<String,String>> lists;
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			User user = (User) msg.obj;
			List<LNG> lngs = user.getLngs();
			
			username = user.getUsername();
			
			lists = new ArrayList<Map<String,String>>();
			String[] num = new String[lngs.size()];
			String[] yw = new String[lngs.size()];
			String[] yl = new String[lngs.size()];
			String[] zkd = new String[lngs.size()];
			String[] qhl = new String[lngs.size()];
			String[] jqcs = new String[lngs.size()];
			String[] ids = new String[lngs.size()];
			for (int i = 0; i < lngs.size(); i++) {
				ids[i] = lngs.get(i).getId();
				num[i] = lngs.get(i).getNum()+"";
				yw[i] = lngs.get(i).getYw();
				yl[i] = lngs.get(i).getYl();
				zkd[i] = lngs.get(i).getZkd();
				qhl[i] = lngs.get(i).getQhl();
				jqcs[i] = lngs.get(i).getJqcs();
			}
			for (int j = 0; j < jqcs.length; j++) {
				Map<String, String> map = new HashMap<>();
				map.put("id", ids[j]);
				map.put("num", num[j]);
				map.put("yw", yw[j]);
				map.put("yl", yl[j]);
				map.put("zkd", zkd[j]);
				map.put("qhl", qhl[j]);
				map.put("jqcs", jqcs[j]);
				lists.add(map);
			}
			
			SimpleAdapter sa = new ShowLngDataAdapter(getApplicationContext(), lists, R.layout.showlng_item, 
					new String[]{"num","yw","yl","zkd","qhl","jqcs"}, new int[]{R.id.id,R.id.yw,R.id.yl,R.id.zkd,R.id.qhl,R.id.jqcs});
			lv.setAdapter(sa);
		};
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_showlngdata);
		lv = (ListView) findViewById(R.id.lv);
		this.setTitle("LNG车载气瓶信息");
		//加载数据
		LngInfoAdapter lngInfoAdapter = new LngInfoAdapter(getApplicationContext(), getIntent().getStringExtra("username"), lv, handler);
		lngInfoAdapter.getUser();
		//设置监听
		setListener();
	}

	private void setListener() {
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					final int position, long id) {
				AlertDialog.Builder builder = new AlertDialog.Builder(ShowLNGDataActivity.this);
				builder.setTitle("选择操作")
						.setItems(new String[]{"增加数据","修改数据","删除数据"}, new OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								switch (which) {
								case 0://增加lng数据
									Intent intent = new Intent(ShowLNGDataActivity.this,AddLNGActivity.class);
									intent.putExtra("username", username);
								 	startActivity(intent);
									ShowLNGDataActivity.this.finish();
									break;
								case 1://修改lng数据
									String id = lists.get(position).get("id");
									Toast.makeText(ShowLNGDataActivity.this, "无权限修改此数据，请与管理员联系...", 1).show();
									break;
								case 2://删除lng数据
									id = lists.get(position).get("id");
									AsyncHttpClient client = new AsyncHttpClient();
									String url = "http://192.168.191.1:8888/LngWebService1/LNGService?id="+id;
									client.get(url, new AsyncHttpResponseHandler() {
										
										@Override
										public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
											if(statusCode == 200){
												String result;
												try {
													result = new String(responseBody,"gbk");
													if(result.equals("删除成功")){
														Intent intent = new Intent(ShowLNGDataActivity.this,MainActivity.class);
														intent.putExtra("username", username);
													 	startActivity(intent);
													 	ShowLNGDataActivity.this.finish();
													 	Toast.makeText(ShowLNGDataActivity.this, "删除成功", 1).show();
													}else if(result.equals("删除失败")){
														Toast.makeText(ShowLNGDataActivity.this, "删除失败", 1).show();
														return;  
													}
												} catch (UnsupportedEncodingException e) {
													e.printStackTrace();
												}
											}
										}
										
										@Override
										public void onFailure(int statusCode, Header[] headers,
												byte[] responseBody, Throwable error) {
											Toast.makeText(ShowLNGDataActivity.this, "删除失败", 1).show();
										}
									});
									break;
								}
							}
						})
						.setNegativeButton("确定", null).show();
				return false;
			}
			
		});
		
	}
	
	
}
