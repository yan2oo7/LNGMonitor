package com.lissic.lng.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lissic.lng.main.R;

public class LngBussines extends Activity{
	
	private WebView webview;
	private ProgressBar pb1;
	private TextView loading;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lngbussines);
		this.setTitle("LNG行业直通车");
		webview = (WebView) findViewById(R.id.bussines);
		pb1 = (ProgressBar) findViewById(R.id.pb1);
		loading = (TextView) findViewById(R.id.loading1);
		WebSettings settings = webview.getSettings();
		settings.setBuiltInZoomControls(true);  
		webview.loadUrl("http://www.lngz.cn/");
		webview.setWebViewClient(new webViewClient());
	}
	 @Override   
    //设置回退    
    //覆盖Activity类的onKeyDown(int keyCoder,KeyEvent event)方法    
    public boolean onKeyDown(int keyCode, KeyEvent event) {    
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {    
            webview.goBack(); //goBack()表示返回WebView的上一页面    
            return true;    
        }    
        finish();//结束退出程序  
        return false;    
    }    
	        
    //Web视图    
    private class webViewClient extends WebViewClient {  
    	
        public boolean shouldOverrideUrlLoading(WebView view, String url) {    
            view.loadUrl(url);    
            return true;    
        }  
        
        @Override
        public void onPageFinished(WebView view, String url) {
        	super.onPageFinished(view, url);
        	pb1.setVisibility(View.GONE);
        	loading.setVisibility(View.GONE);
        }
        
    }    
}
