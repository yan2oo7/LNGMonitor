package com.lissic.lng.activities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.lissic.lng.entity.NewsItem;
import com.lissic.lng.main.R;
import com.lissic.lng.utils.NewsSpiderUtil;
import com.lissic.lng.utils.ShowContent;

public class NewsActivity extends Activity {

	private ProgressBar pb;
	private TextView loading;
	private ListView news;
	String url="http://www.cnlng.com/list.php?fid=2";
	String url1 = "http://www.cnlng.com";
	private List<NewsItem> list;
	private String[] titles = new String[16*2];
	private String[] contents = new String[16*2];
	private String[] subTimes = new String[16*2];
	private String[] clickTimes = new String[16*2];
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);
		pb = (ProgressBar) findViewById(R.id.progressBar1);
		news = (ListView) findViewById(R.id.lv_news);
		loading = (TextView) findViewById(R.id.loading);
		DownTask task = new DownTask();
		task.execute();
	}
	
	public void back(View v){
		Intent intent = new Intent(NewsActivity.this, MainActivity.class);
		startActivity(intent);
		NewsActivity.this.finish();
	}
	
	class DownTask extends AsyncTask<Void, Integer, List<NewsItem>>{
		
		@Override
		protected List<NewsItem> doInBackground(Void... params) {
			list = new ArrayList<>();
			try {
				List<NewsItem> list_news = NewsSpiderUtil.getNewsItem();
				list.addAll(list_news);
			} catch (IOException e) {
				e.printStackTrace();
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						//不显示listView
						news.setVisibility(View.GONE);
						Toast.makeText(getApplicationContext(), "网络错误。。。", 1).show();
					}
				});
			}
			return list;
		}
		
		@Override
		protected void onPostExecute(final List<NewsItem> list) {
			for (int i = 0; i < list.size(); i++) {
				titles[i] = list.get(i).getTitle();
				contents[i] = list.get(i).getContent();
				subTimes[i] = "发布时间:"+list.get(i).getSubTime();
				clickTimes[i] = "点击量:"+list.get(i).getClickTimes();
System.out.println(titles[i]+contents[i]+clickTimes[i]+subTimes[i]);
			}
			
			List<Map<String,Object>> lists = new ArrayList<Map<String,Object>>();
			for (int i = 0; i < titles.length; i++) {
				Map<String, Object> news = new HashMap<String, Object>();
				news.put("title", titles[i]);
				news.put("content", contents[i]);
				news.put("subTime", subTimes[i]);
				news.put("clickTime", clickTimes[i]);
				lists.add(news);
			}
			
			SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), 
					lists, R.layout.news_item, new String[]{"title","content","subTime","clickTime"}
						,new int[]{R.id.title,R.id.news_content,R.id.subTime,R.id.clickTimes});
			news.setAdapter(adapter);
			pb.setVisibility(View.GONE);
			loading.setVisibility(View.GONE);
			//设置新闻列表的点击事件(点击条目后展示新闻的详情)
			news.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					//取出newItem中新闻详情的地址
					String contentUrl = list.get(position).getLink();
					String title = list.get(position).getTitle();
					Intent intent = new Intent(getApplicationContext(), NewsDetailActivity.class);
					intent.putExtra("contentUrl", contentUrl);
					intent.putExtra("title", title);
					startActivity(intent);
				}
				
			});
		}
		
	}
	
	
}
