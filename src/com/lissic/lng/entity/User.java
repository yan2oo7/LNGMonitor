package com.lissic.lng.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class User implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String username;
	private String password;
	private String sex;
	private String tel;
	private String address;
	private String carId;
	private Date registTime;
	private int isAdamin;
	private List<LNG> lngs;
	public User() {
		super();
	}
	public User(int id, String username, String password, String sex,
			String tel, String address, String carId, Date registTime,
			int isAdamin, List<LNG> lngs) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.sex = sex;
		this.tel = tel;
		this.address = address;
		this.carId = carId;
		this.registTime = registTime;
		this.isAdamin = isAdamin;
		this.lngs = lngs;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCarId() {
		return carId;
	}
	public void setCarId(String carId) {
		this.carId = carId;
	}
	public Date getRegistTime() {
		return registTime;
	}
	public void setRegistTime(Date registTime) {
		this.registTime = registTime;
	}
	public int getIsAdamin() {
		return isAdamin;
	}
	public void setIsAdamin(int isAdamin) {
		this.isAdamin = isAdamin;
	}
	public List<LNG> getLngs() {
		return lngs;
	}
	public void setLngs(List<LNG> lngs) {
		this.lngs = lngs;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password="
				+ password + ", sex=" + sex + ", tel=" + tel + ", address="
				+ address + ", carId=" + carId + ", registTime=" + registTime
				+ ", isAdamin=" + isAdamin + ", lngs=" + lngs + "]";
	}
	
}
