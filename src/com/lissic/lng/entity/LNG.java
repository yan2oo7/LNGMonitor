package com.lissic.lng.entity;

import java.io.Serializable;

public class LNG implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int num;
	private String yw;
	private String yl;
	private String zkd;
	private String qhl;
	private String jqcs;
	private String id;
	
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public LNG() {
		super();
	}
	public LNG(int num, String yw, String yl, String zkd, String qhl, String jqcs) {
		super();
		this.num = num;
		this.yw = yw;
		this.yl = yl;
		this.zkd = zkd;
		this.qhl = qhl;
		this.jqcs = jqcs;
	}
	
	public String getYw() {
		return yw;
	}
	public void setYw(String yw) {
		this.yw = yw;
	}
	public String getYl() {
		return yl;
	}
	public void setYl(String yl) {
		this.yl = yl;
	}
	public String getZkd() {
		return zkd;
	}
	public void setZkd(String zkd) {
		this.zkd = zkd;
	}
	public String getQhl() {
		return qhl;
	}
	public void setQhl(String qhl) {
		this.qhl = qhl;
	}
	public String getJqcs() {
		return jqcs;
	}
	public void setJqcs(String jqcs) {
		this.jqcs = jqcs;
	}
	@Override
	public String toString() {
		return "LNG [id=" + num + ", yw=" + yw + ", yl=" + yl + ", zkd=" + zkd
				+ ", qhl=" + qhl + ", jqcs=" + jqcs + "]";
	}
	
	
}
