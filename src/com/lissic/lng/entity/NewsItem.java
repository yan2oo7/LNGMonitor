package com.lissic.lng.entity;

public class NewsItem {
	
	private String title;
	private String content;
	private String subTime;
	private String link;
	private String clickTimes;
	public NewsItem(String title, String content, String subTime, String link,
			String clickTimes) {
		super();
		this.title = title;
		this.content = content;
		this.subTime = subTime;
		this.link = link;
		this.clickTimes = clickTimes;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSubTime() {
		return subTime;
	}
	public void setSubTime(String subTime) {
		this.subTime = subTime;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getClickTimes() {
		return clickTimes;
	}
	public void setClickTimes(String clickTimes) {
		this.clickTimes = clickTimes;
	}
	@Override
	public String toString() {
		return "NewsItem [title=" + title + ", content=" + content
				+ ", subTime=" + subTime + ", link=" + link + ", clickTimes="
				+ clickTimes + "]";
	}
	
	
}
