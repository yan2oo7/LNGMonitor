package com.lissic.lng.utils;

import android.content.Context;
import android.widget.Toast;

public class ShowContent {
	private Context context;
	//在非UI线程中将数据展示到Toast上
	public ShowContent(Context context){
		this.context = context;
	}
	public void  showContent(final String content){
			new Thread(){
				@Override
				public void run() {
					Toast.makeText(context, content, 1).show();
				}
			}.start();
		}
}
