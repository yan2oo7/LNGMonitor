package com.lissic.lng.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.os.Handler;
import android.os.Message;

import com.lissic.lng.entity.NewsItem;


public class NewsSpiderUtil {
	
	private static String url = "http://www.cnlng.com/list.php?fid=2";
	private static String url2 ="http://www.cnlng.com";
	private static List<NewsItem> list = new ArrayList<>();
	private static Document doc = null;
	public static List<NewsItem> getNewsItem() throws IOException{
		
			doc = Jsoup.connect(url).get();
			Elements elements = doc.select("div.cont");
			Elements table = elements.select("div.cont > table");
			Elements tbodys = table.select("table > tbody");
			for (int i = 0; i < 16; i++) {
				Element tbody = tbodys.get(i);
				Elements tds = tbody.getElementsByTag("td");
				String title = tds.get(0).getElementsByTag("u").text();
				String content = tds.get(1).text();
				Elements hrefs = tds.get(2).select("a[href]");
				String href = hrefs.get(0).attr("href");
				String contenturl = url2 + "/" +href; 
				String subTime = tds.get(2).getElementsByTag("a").get(1).text();
				
				NewsItem news = new NewsItem(title, content, subTime, contenturl, "0");
				list.add(news);
			}
		
		return list;
	}
	
	/**
	 * 获取新闻的详细信息
	 * @return
	 * @throws IOException 
	 */
	public static String getNewsContent(String contentUrl) throws IOException{
		
		Document doc = Jsoup.connect(contentUrl).get();
		Element span = doc.getElementById("post1");
		Elements content = span.select("div");
		String text = content.text();
		return text;
		
	}
	
}
