package com.lissic.lng.utils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class StreamTools {
	public static String readStream(InputStream in) throws Exception{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		int len = -1;
		byte[] buffer = new byte[1024];
		if((len=in.read(buffer))!=-1){
			baos.write(buffer, 0, len);
		}
		String content = new String(baos.toByteArray(),"gbk");
		return content;
	}
}
