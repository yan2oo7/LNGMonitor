package com.lissic.lng.utils;

import java.io.UnsupportedEncodingException;

import org.apache.http.Header;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;


import android.os.Handler;
import android.os.Message;

public class HttpUtil {
	private static String path="";
	/**
	 * 登录成功
	 * @param username
	 * @param password
	 */
	public static void loginSuccess(final String username, final String password,final String lisence, final Handler handler) {
		path = "http://192.168.191.1:8888/LngWebService1/LoginService?username="+username+"&password="+password+""+"&lisence="+lisence;
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(path, new AsyncHttpResponseHandler() {
			//请求成功时回调的方法
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				String content;
				try {
					content = new String(responseBody,"gbk");
					Message msg = Message.obtain();
					if(content.equals("成功")){
						msg.obj = "success";
					}else if(content.equals("密码错误")){
						msg.obj = "fail";
					}else{
						msg.obj = "notexits";
					}
					handler.sendMessage(msg);
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				
			}
			//请求失败时回调的方法
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				Message msg = Message.obtain();
				msg.obj=statusCode;
				handler.sendMessage(msg);
			}
		});
	}
	
}
