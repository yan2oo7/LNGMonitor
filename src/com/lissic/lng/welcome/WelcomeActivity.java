package com.lissic.lng.welcome;

import com.lissic.lng.activities.LoginActivity;
import com.lissic.lng.main.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class WelcomeActivity extends Activity{
  
	private static final int SPLASH_DISPLAY_LENGTH = 3000;//延时3秒钟
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//加载欢迎页面
		setContentView(R.layout.activity_welcome);
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				//开启mainactivity
				Intent intent = new Intent(WelcomeActivity.this,LoginActivity.class);
				WelcomeActivity.this.startActivity(intent);
				//关闭欢迎页面
				WelcomeActivity.this.finish();
				
			}
		}, SPLASH_DISPLAY_LENGTH);
	}
	
	
}
