package com.lissic.lng.dao;

import com.lissic.lng.entity.LNG;

/**
 * 对lng气瓶的一些相关的操作
 */

public interface ILngDao {
	//添加lng气瓶的数据
	public int addLngData(LNG lng);
	
	//删除lng气瓶的数据
	public int deleteLngData(int lng_id);
	
	//修改lng气瓶参数
	public int updateLngData(LNG lng);
	
	//查询lng气瓶的数据
	public LNG queryLngData(int lng_id);
}
