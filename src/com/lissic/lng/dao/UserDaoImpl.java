package com.lissic.lng.dao;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.Header;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateFormat;

import com.lissic.lng.entity.LNG;
import com.lissic.lng.entity.User;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class UserDaoImpl implements IUserDao{
	
	private AsyncHttpClient client ;
	private RequestParams params;
	private Handler handler;
	private Context context;
	private String url="http://192.168.191.1:8888/LngWebService1/userService";
	private int flag;
	
	private User user = new User();
	private LNG lng;
	public UserDaoImpl(Context context,Handler handler) {
		this.context = context;
		this.handler = handler;
	}
	
	public UserDaoImpl() {
	}

	@Override
	public int addUser(User user) {
		client = new AsyncHttpClient();
		params = new RequestParams();
		LNG lng = user.getLngs().get(0);
		params.put("username", user.getUsername());
		params.put("password", user.getPassword());
		params.put("sex", user.getSex());
		params.put("tel", user.getTel());
		params.put("addr", user.getAddress());
		params.put("carId", user.getCarId());
		params.put("isAdamin", user.getIsAdamin()+"");
		params.put("lngid", lng.getId());
		params.put("lngyw", lng.getYw());
		params.put("lngyl", lng.getYl());
		params.put("lngzkd", lng.getZkd());
		params.put("lngqhl", lng.getQhl());
		params.put("lngjqcs", lng.getJqcs());
		//检测网络是否连接
		ConnectivityManager connect  = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = connect.getActiveNetworkInfo();
		if(info!=null){
			boolean connected = info.isConnected();
			if(connected){
				client.post(url, params, new AsyncHttpResponseHandler() {
					@Override
					public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
						try {
							String content = new String(responseBody,"gbk");
							Message msg = Message.obtain();
							if(statusCode==200){
								if(content.equals("添加成功")){
									msg.obj = "success";
								}else if(content.equals("添加失败")){
									msg.obj = "fail";
								}
							}
							handler.sendMessage(msg);
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}
					}
					
					@Override
					public void onFailure(int statusCode, Header[] headers,
							byte[] responseBody, Throwable error) {
					}
				});
			}else{
				Message msg = Message.obtain();
				msg.obj = "disconnect";
				handler.sendMessage(msg);
			}
		}else{
			Message msg = Message.obtain();
			msg.obj = "disconnect";
			handler.sendMessage(msg);
		}
		return flag;
	}

	@Override
	public int deleteUser(int id) {
		//实现删除用户的逻辑
		return 0;
	}

	@Override
	public int updateUser(User user) {
		// 实现更新用户的逻辑
		
		return 0;
	}

	@Override
	public User queryUser(String username) {
		url="http://192.168.191.1:8888/LngWebService1/userService?username="+username;
		// 实现查询用户的逻辑
		client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				try {
					String userInfo = new String(responseBody,"gbk");
					String[] info = userInfo.split("/");
					user.setUsername(info[0]);
					user.setSex(info[1]);
					user.setTel(info[2]);
					user.setAddress(info[3]);
					user.setCarId(info[4]);
					user.setIsAdamin(Integer.parseInt(info[5]));
					user.setPassword(info[6]);
					List<LNG> lngs = new ArrayList<>(); 
					if(info.length==14){
						lng = new LNG(Integer.parseInt(info[7]), info[8], info[9], info[10], info[11], info[12]);
						lng.setId(info[13]);
						lngs.add(lng);
					}else if(info.length==21){
						lng = new LNG(Integer.parseInt(info[7]), info[8], info[9], info[10], info[11], info[12]);
						lng.setId(info[13]);
						lngs.add(lng);
						lng = new LNG(Integer.parseInt(info[14]), info[15], info[16], info[17], info[18], info[19]);
						lng.setId(info[20]);
						lngs.add(lng);
					}else if(info.length==28){
						lng = new LNG(Integer.parseInt(info[7]), info[8], info[9], info[10], info[11], info[12]);
						lng.setId(info[13]);
						lngs.add(lng);
						lng = new LNG(Integer.parseInt(info[14]), info[15], info[16], info[17], info[18], info[19]);
						lng.setId(info[20]);
						lngs.add(lng);
						lng = new LNG(Integer.parseInt(info[21]), info[22], info[23], info[24], info[25], info[26]);
						lng.setId(info[27]);
						lngs.add(lng);
					}else if(info.length==35){
						lng = new LNG(Integer.parseInt(info[7]), info[8], info[9], info[10], info[11], info[12]);
						lng.setId(info[13]);
						lngs.add(lng);
						lng = new LNG(Integer.parseInt(info[14]), info[15], info[16], info[17], info[18], info[19]);
						lng.setId(info[20]);
						lngs.add(lng);
						lng = new LNG(Integer.parseInt(info[21]), info[22], info[23], info[24], info[25], info[26]);
						lng.setId(info[27]);
						lngs.add(lng);
						lng = new LNG(Integer.parseInt(info[28]), info[29], info[30], info[31], info[32], info[33]);
						lng.setId(info[34]);
						lngs.add(lng);
					}
					user.setLngs(lngs);
					Message msg = Message.obtain();
					msg.obj = user;
					handler.sendMessage(msg);
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			
			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				
			}
		});
		return user;
	}

}
