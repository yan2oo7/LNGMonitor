package com.lissic.lng.dao;

import com.lissic.lng.entity.User;

/**
 * 对用户的一些相关的操作
 *
 */
public interface IUserDao {
	//增加用户
	public int addUser(User user);
	//删除用户
	public int deleteUser(int id);
	//更新用户的信息
	public int updateUser(User user);
	//查看用户的相关信息
	public User queryUser(String username);
}	
